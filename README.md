# HDIM

HDIM is a collection of packages provided by the
[Lederer and Hauser HDIM Group]( https://lederer.stat.washington.edu/ )

HDIM provides fast methods to perform high-dimensional linear regression --
most notably the FOS ( Fast and Optimal Selection ) method for minimizing the
 LASSO objective function.

# Dependencies

### Base

You will need to have the following packages installed in order to use the base HDIM package:

* [Eigen3](http://eigen.tuxfamily.org/index.php?title=Main_Page)

### Testing Framework

* [Armadillo](http://arma.sourceforge.net/download.html)

### Python Wrapper

* [SWIG](http://www.swig.org/download.html) The Simplified Wrapper and Interface Generator
* [Python Development Headers]

### R Wrapper

* [Rcpp](https://cran.r-project.org/web/packages/Rcpp/index.html)

### GPU Acceleration

To use GPU accelerated functions you will need a working OpenCL implementation and the following:

* [clBLAS](https://github.com/clMathLibraries/clBLAS)

## Authors

* **Benjamin J Phillips** - *Initial work*

## References

[FOS](https://arxiv.org/abs/1609.07195)
