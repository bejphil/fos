var hierarchy =
[
    [ "hdim::Binarize< T >", "structhdim_1_1_binarize.html", null ],
    [ "hdim::FOS< T >", "classhdim_1_1_f_o_s.html", null ],
    [ "ocl::OpenCLBase", "classocl_1_1_open_c_l_base.html", [
      [ "MatProdTest", "class_mat_prod_test.html", null ],
      [ "MatVectProdTest", "class_mat_vect_prod_test.html", null ]
    ] ],
    [ "hdim::SoftThres< T >", "structhdim_1_1_soft_thres.html", null ],
    [ "hdim::internal::SubGradientSolver< T >", "classhdim_1_1internal_1_1_sub_gradient_solver.html", [
      [ "hdim::FISTA< T >", "classhdim_1_1_f_i_s_t_a.html", null ],
      [ "hdim::ISTA< T >", "classhdim_1_1_i_s_t_a.html", null ]
    ] ],
    [ "hdim::SupportSift< T >", "structhdim_1_1_support_sift.html", null ],
    [ "hdim::experimental::X_FOS< T >", "classhdim_1_1experimental_1_1_x___f_o_s.html", null ]
];