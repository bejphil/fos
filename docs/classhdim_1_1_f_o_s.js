var classhdim_1_1_f_o_s =
[
    [ "FOS", "classhdim_1_1_f_o_s.html#aedf0da3eaea4dd21392deffc10b175e4", null ],
    [ "Algorithm", "classhdim_1_1_f_o_s.html#a51b4de321e8bed46676fa6d3d938c5cb", null ],
    [ "ReturnBetas", "classhdim_1_1_f_o_s.html#a0abda497cacc597cce94e8be83cb8340", null ],
    [ "ReturnCoefficients", "classhdim_1_1_f_o_s.html#ad3da00c1d526f1094cccf3bc5992be00", null ],
    [ "ReturnLambda", "classhdim_1_1_f_o_s.html#abe7909f68be24543961afbda9dd9e1cf", null ],
    [ "ReturnOptimIndex", "classhdim_1_1_f_o_s.html#a60634d7b70519381c48680098a702010", null ],
    [ "ReturnSupport", "classhdim_1_1_f_o_s.html#af32e017150aaddc34e63723868688f8e", null ],
    [ "avfos_fit", "classhdim_1_1_f_o_s.html#a6ecb10990ce53ad1d75dadc1a0b92c4c", null ],
    [ "lambda", "classhdim_1_1_f_o_s.html#afef69fbe688335cca25dc2af2834bdcb", null ],
    [ "optim_index", "classhdim_1_1_f_o_s.html#a424e65567c6187b14f6f581531cbeebf", null ]
];