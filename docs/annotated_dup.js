var annotated_dup =
[
    [ "hdim", null, [
      [ "experimental", null, [
        [ "X_FOS", "classhdim_1_1experimental_1_1_x___f_o_s.html", "classhdim_1_1experimental_1_1_x___f_o_s" ]
      ] ],
      [ "internal", null, [
        [ "SubGradientSolver", "classhdim_1_1internal_1_1_sub_gradient_solver.html", "classhdim_1_1internal_1_1_sub_gradient_solver" ]
      ] ],
      [ "Binarize", "structhdim_1_1_binarize.html", "structhdim_1_1_binarize" ],
      [ "FISTA", "classhdim_1_1_f_i_s_t_a.html", "classhdim_1_1_f_i_s_t_a" ],
      [ "FOS", "classhdim_1_1_f_o_s.html", "classhdim_1_1_f_o_s" ],
      [ "ISTA", "classhdim_1_1_i_s_t_a.html", "classhdim_1_1_i_s_t_a" ],
      [ "SoftThres", "structhdim_1_1_soft_thres.html", "structhdim_1_1_soft_thres" ],
      [ "SupportSift", "structhdim_1_1_support_sift.html", "structhdim_1_1_support_sift" ]
    ] ],
    [ "ocl", null, [
      [ "OpenCLBase", "classocl_1_1_open_c_l_base.html", "classocl_1_1_open_c_l_base" ]
    ] ],
    [ "MatProdTest", "class_mat_prod_test.html", "class_mat_prod_test" ],
    [ "MatVectProdTest", "class_mat_vect_prod_test.html", "class_mat_vect_prod_test" ]
];