var classhdim_1_1experimental_1_1_x___f_o_s =
[
    [ "X_FOS", "classhdim_1_1experimental_1_1_x___f_o_s.html#a1c7804223ae195cd33864303a301d86e", null ],
    [ "operator()", "classhdim_1_1experimental_1_1_x___f_o_s.html#ab94de50897bfeeeaf3f3793c02e80a0d", null ],
    [ "ReturnBetas", "classhdim_1_1experimental_1_1_x___f_o_s.html#ab8117b36da518aad94704ff8fdf7cf13", null ],
    [ "ReturnCoefficients", "classhdim_1_1experimental_1_1_x___f_o_s.html#a8b8846a478bd6d77716ea3a094cfa133", null ],
    [ "ReturnLambda", "classhdim_1_1experimental_1_1_x___f_o_s.html#abc38a86700b585a621b6a5156858e5d2", null ],
    [ "ReturnOptimIndex", "classhdim_1_1experimental_1_1_x___f_o_s.html#ab608fdf34957be02d08063e20e82cff3", null ],
    [ "ReturnSupport", "classhdim_1_1experimental_1_1_x___f_o_s.html#aa719f2746cbaf70ab2bb051aaf682a98", null ],
    [ "fos_fit", "classhdim_1_1experimental_1_1_x___f_o_s.html#a3ec2c8563ce047cf5c2d34ace022351f", null ],
    [ "lambda", "classhdim_1_1experimental_1_1_x___f_o_s.html#ab970a7326cf78df3c54af7ab34c2e631", null ],
    [ "optim_index", "classhdim_1_1experimental_1_1_x___f_o_s.html#a877d7da4fbf355432224c5716761362b", null ]
];