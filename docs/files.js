var files =
[
    [ "docs", "dir_49e56c817e5e54854c35e136979f97ca.html", "dir_49e56c817e5e54854c35e136979f97ca" ],
    [ "FOS", "dir_01b1b69ac081d0825ad7ccac6367cb53.html", "dir_01b1b69ac081d0825ad7ccac6367cb53" ],
    [ "Generic", "dir_d5bac1d80f65f7075574489e24503a17.html", "dir_d5bac1d80f65f7075574489e24503a17" ],
    [ "OpenCL_Base", "dir_b34738e4897d4fab4ca811e2d3b116f4.html", "dir_b34738e4897d4fab4ca811e2d3b116f4" ],
    [ "OpenCL_Generics", "dir_2c6f6aa72448d67687ebd00140699186.html", "dir_2c6f6aa72448d67687ebd00140699186" ],
    [ "Solvers", "dir_bdfa83abd3770e9236d0774c4efb3ff7.html", "dir_bdfa83abd3770e9236d0774c4efb3ff7" ],
    [ "SPAMS", "dir_46c2e4c76a8789d3d9dfe4582751439f.html", "dir_46c2e4c76a8789d3d9dfe4582751439f" ],
    [ "main.cpp", "main_8cpp_source.html", null ],
    [ "test_armadillo.h", "test__armadillo_8h_source.html", null ],
    [ "test_eigen3.h", "test__eigen3_8h_source.html", null ]
];