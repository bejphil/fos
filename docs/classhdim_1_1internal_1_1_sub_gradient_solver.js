var classhdim_1_1internal_1_1_sub_gradient_solver =
[
    [ "SubGradientSolver", "classhdim_1_1internal_1_1_sub_gradient_solver.html#a25e439cda3fbbfada1a5e5db8fb513df", null ],
    [ "f_beta", "classhdim_1_1internal_1_1_sub_gradient_solver.html#a5001210a95dfd5c2bdd39200d7c6fda3", null ],
    [ "f_beta_tilda", "classhdim_1_1internal_1_1_sub_gradient_solver.html#a9f2e40570dc82e3fdb84040eaa204548", null ],
    [ "operator()", "classhdim_1_1internal_1_1_sub_gradient_solver.html#aeb015a88e4ff06cf7a106d6beb7a5245", null ],
    [ "operator()", "classhdim_1_1internal_1_1_sub_gradient_solver.html#a0c2d469428f5c344318137c282155ea2", null ],
    [ "update_beta_ista", "classhdim_1_1internal_1_1_sub_gradient_solver.html#a7d954caffc52795f495f6a6f4d9efe4c", null ]
];