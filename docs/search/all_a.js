var searchData=
[
  ['normalize',['Normalize',['../generics_8h.html#a47c21fd65c6b6f574eebdba68c6a19f6',1,'hdim::Normalize(const Eigen::Matrix&lt; T, Eigen::Dynamic, Eigen::Dynamic &gt; &amp;mat)'],['../generics_8h.html#ac19e22668ab6058b56125bb2d787f2d5',1,'hdim::Normalize(const Eigen::Matrix&lt; T, Eigen::Dynamic, 1 &gt; &amp;mat)'],['../generics_8h.html#a3d4a53a1b93612e5dd9147aed0b45d13',1,'hdim::Normalize(Eigen::Matrix&lt; T, Eigen::Dynamic, 1 &gt; &amp;mat)']]],
  ['normalize_5fip',['Normalize_IP',['../generics_8h.html#a0cf7363585fe9ae6d8bf8aae1035757e',1,'hdim::Normalize_IP(Eigen::Matrix&lt; T, Eigen::Dynamic, Eigen::Dynamic &gt; &amp;mat)'],['../generics_8h.html#a9a577c7b3f249973cdbef71c146f934c',1,'hdim::Normalize_IP(Eigen::Matrix&lt; T, Eigen::Dynamic, 1 &gt; &amp;mat)']]]
];
