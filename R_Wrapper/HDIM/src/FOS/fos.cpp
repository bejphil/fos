//Explicit Instantiation of FOS for Python Wrappers

#include "fos.h"

template class hdim::FOS<float>;
template class hdim::FOS<double>;
