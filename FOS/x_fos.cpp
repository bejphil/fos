//Explicit Instantiation of X_FOS for Python Wrappers

#include "x_fos.h"

template class hdim::experimental::X_FOS<float>;
template class hdim::experimental::X_FOS<double>;
